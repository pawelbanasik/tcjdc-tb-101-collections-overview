package com.pawelbanasik;

public class Main {

	public static void main(String[] args) {

		Theatre theatre = new Theatre("Olympian", 8, 12);
//		 theatre.getSeats();

		if (theatre.reserveSeat("H11")) {
			System.out.println("Please pay");

		} else {
			System.out.println("Sorry, seat is taken.");
		}

		if (theatre.reserveSeat("H11")) {
			System.out.println("Please pay");

		} else {
			System.out.println("Sorry, seat is taken.");
		}
	
		// fajna metoda do fomatowania z leading zero
		// int a = 1;
		// System.out.println(String.format("%02d", a));
		
		// char ma wartosc liczbowa
		// int b = 'Z';
		// System.out.println(b);
		// int a = 'Z' + 8;
		// System.out.println(a);
		
		// for (char i = 'A'; i < 'Z'; i++) {
		// System.out.println(i);
		// }
	}
	


}
